public class Meja {
    private int nomorMeja;
    private Pelanggan pelanggan;
    private Menu[] menu;
    
    public Meja(int nomorMeja) {
        this.nomorMeja = nomorMeja;
        this.menu = new Menu[10]; // maksimum 10 pesanan per meja
    }

    // mengambil nilai nomor meja
    public int getNomorMeja() {
        return nomorMeja;
    }

    // merubah nilai nomor meja
    public void setNomorMeja(int nomorMeja) {
        this.nomorMeja = nomorMeja;
    }

    // mengambil nilai pelanggan
    public Pelanggan getPelanggan() {
        return pelanggan;
    }

    // merubah nilai pelanggan   
    public void setPelanggan(Pelanggan pelanggan) {
        this.pelanggan = pelanggan;
    }

    // mengambil nilai menu
    public Menu[] getMenu() {
        return menu;
    }

    //digunakan untuk memilih daftar menu dan membuat batas jumlah menu yang bisa dipesan 
    public void setMenu(Menu menu) {
        boolean pesananDitambahkan = false;
        for (int i = 0; i < this.menu.length; i++) {
            if (this.menu[i] == null) {
                this.menu[i] = menu;
                pesananDitambahkan = true;
                break;
            }
        }
        if (!pesananDitambahkan) {
            System.out.println("Maaf, tidak dapat menambahkan pesanan lagi. Sudah mencapai batas maksimum 10 pesanan.");
        }
    }
    
    // untuk mengecek apakah meja kosong
    public boolean isKosong() {
        //EDIT DISINI
        if (getPelanggan()==null) {
            return true;
        }
        return false;
    }
}
